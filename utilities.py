import torch
import torch.nn as nn
import numpy as np
import os

# GLOBAL FUNCTION

def file_exists(name):
    for file in os.listdir(PROJECT_PATH):
        if file.__contains__(name):
            return True
    return False

# CONSTANT VALUES
PROJECT_PATH = os.path.abspath(os.getcwd())
EDGE_FILTER = [-1, 0, 1]
BLUR_FILTER = [1, 1, 1]

NN_INPUT_SIZE = 100
NN_HIDDEN_SIZE = 25
NN_OUTPUT_SIZE = 100

ZONE_SIZE = 10


# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Global variables
num_epochs = 5
batch_size = 100
learning_rate = 0.001


# Fully connected neural network with expandable structure
class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(NeuralNet, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        out = self.relu(self.fc1(x))
        out = self.fc2(out)
        return out

    def object_transform(self, obj):
        tensor = torch.from_numpy(np.asarray(obj))
        tensor = tensor.type(dtype=torch.float32)
        tensor.retain_grad()
        return tensor


class Reader(torch.utils.data.Dataset):
    def __init__(self):

        self.inputMatrix = []
        self.doctorInputMatrix = []
        self.outputMatrix = []
        self.path = ""
        self.counter = -1

    def get_all_files(self):
        return self.inputMatrix, self.doctorInputMatrix, self.outputMatrix

    def read_files(self, folderNumber):

        folderPath = ""

        self.counter += 1

        if folderNumber == 1:
            folderPath = PROJECT_PATH + "\input"
        else:
            folderPath = PROJECT_PATH + "\input" + str(folderNumber)

        for file in os.listdir(folderPath):

            filePath = None
            objectType = None

            if file.__contains__("HU") and file.endswith('.in'):
                # tc image matrix
                filePath = os.path.join(folderPath, file)
                objectType = 0

            if file.__contains__("seg") and file.endswith('.in'):
                # doctor segmentation matrix
                filePath = os.path.join(folderPath, file)
                objectType = 1

            if file.__contains__("opt") and file.endswith('.out'):
                # optimized segmentation matrix
                filePath = os.path.join(folderPath, file)
                objectType = 2

            if filePath is not None:
                with open(filePath) as fileObject:
                    if objectType == 0:
                        self.inputMatrix.append(fileObject.readlines())
                    elif objectType == 1:
                        self.doctorInputMatrix.append(fileObject.readlines())
                    elif objectType == 2:
                        self.outputMatrix.append(fileObject.readlines())

        self.process_files()

    def process_files(self):
        inMat = self.inputMatrix[self.counter]
        outMat = self.outputMatrix[self.counter]
        inDocMat = self.doctorInputMatrix[self.counter]

        self.inputMatrix[self.counter] = self.process_object(inMat)
        self.outputMatrix[self.counter] = self.process_object(outMat)
        self.doctorInputMatrix[self.counter] = self.process_object(inDocMat)

    def process_object(self, obj):

        for i in range(len(obj)):
            line = obj[i]
            line = line.replace("\n", "")
            line = [float((int(x) + 3024) / 6095) for x in line.split()]
            obj[i] = line

        return obj


class Transformer:
    def __init__(self, pack):

        self.inputFile, self.inputDocFile, self.outputFile = pack

        self.model = NeuralNet(NN_INPUT_SIZE, NN_HIDDEN_SIZE, NN_OUTPUT_SIZE)
        self.lossFunction = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=learning_rate)

    def get_zone_pair(self, x, y, inObj, outObj):

        inputArray = []
        outputArray = []

        for i in range(x, x + ZONE_SIZE):
            for j in range(y, y + ZONE_SIZE):
                inputArray.append(inObj[i][j])
                outputArray.append(outObj[i][j])

        return inputArray, outputArray

    def get_single_zone_pair(self, x, y, inObj):

        inputArray = []

        for i in range(x, x + ZONE_SIZE):
            for j in range(y, y + ZONE_SIZE):
                inputArray.append(inObj[i][j])

        return inputArray

    def train_for_zones(self, counter):

        inputObject = self.transform_input(counter)
        outputObject = self.outputFile[counter]

        for i in range(0, len(inputObject) - ZONE_SIZE, ZONE_SIZE):
            for j in range(0, len(inputObject) - ZONE_SIZE, ZONE_SIZE):

                inObj, outObj = self.get_zone_pair(i, j, inputObject, outputObject)

                inObj = self.model.object_transform(inObj)
                inObj = inObj.reshape(1, -1).squeeze().to(device)
                outObj = self.model.object_transform(outObj).reshape(1, -1).squeeze().to(device)

                obtainedOutput = self.model(inObj)
                #print(obtainedOutput)
                loss = self.lossFunction(outObj, obtainedOutput)

                # Backward and optimize
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

        torch.save(self.model.state_dict(), "model.pkl")

    def test_for_zones(self, counter):

        self.restore_parameters()

        inputObject = self.transform_input(counter)
        outputObject = []

        for i in range(0, len(inputObject) - ZONE_SIZE, ZONE_SIZE):
            for j in range(0, len(inputObject) - ZONE_SIZE, ZONE_SIZE):

                inObj = self.get_single_zone_pair(i, j, inputObject)

                inObj = self.model.object_transform(inObj)
                inObj = inObj.reshape(1, -1).squeeze().to(device)

                obtainedOutput = self.model(inObj)
                outputObject.append(obtainedOutput.tolist())

        return counter, outputObject

    def restore_parameters(self):
        self.model = NeuralNet(NN_INPUT_SIZE, NN_HIDDEN_SIZE, NN_OUTPUT_SIZE)
        self.model.load_state_dict(torch.load('model.pkl'))

    def transform_input(self, counter):
        self.inputDocFile[counter] = self.apply_filter(self.inputDocFile[counter], EDGE_FILTER)
        self.inputDocFile[counter] = self.apply_filter(self.inputDocFile[counter], BLUR_FILTER)
        return self.merge_objects(self.inputFile[counter], self.inputDocFile[counter])

    def get_output_file(self, object):
        counter, outputObject = object
        #print(counter, "0")
        name = "output" + str(counter) + ".txt"
        with open(name, "w") as file:
            for line in outputObject:
                line = self.transform_line(line)
                file.write(line)

    def transform_line(self, line):
        message = ""
        for elem in line:
            if elem > 0.5:
                message += "1 "
            else:
                message += "0 "
        message = message.rstrip(" ")
        message += "\n"
        return message

    def print_doc(self, counter):
        for line in self.inputDocFile[counter]:
            for obj in line:
                if obj != 0:
                    print("yes")
            print(line)

    def merge_objects(self, obj1, obj2):

        for i in range(len(obj1)):
            for j in range(len(obj1)):
                obj1[i][j] *= obj2[i][j]

        return obj1

    def apply_filter(self, obj, filter):

        for i in range(len(obj)):
            line = np.asarray(obj[i])
            line = np.convolve(filter, line, "same")
            obj[i] = np.ndarray.tolist(line)

        return obj