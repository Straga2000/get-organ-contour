from utilities import *

read = Reader()

for i in range(1, 5):
    read.read_files(i)
    print(i)

transform = Transformer(read.get_all_files())
for i in range(1, 4):
    if file_exists('model.pkl'):
        transform.get_output_file(transform.test_for_zones(i))
    else:
        transform.train_for_zones(i)
        transform.get_output_file(transform.test_for_zones(i))
